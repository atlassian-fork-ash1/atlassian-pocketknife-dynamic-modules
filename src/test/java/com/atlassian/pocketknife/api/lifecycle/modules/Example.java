package com.atlassian.pocketknife.api.lifecycle.modules;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;

/**
 * Some example code so we can document it better and yet it compiles as expected
 */
@SuppressWarnings("ALL")
public class Example {

    private PluginAccessor pluginAccessor; // Spring injected
    private DynamicModuleDescriptorFactory dynamicModuleDescriptorFactory; // Spring injected

    private ModuleRegistrationHandle registrationHandle;

    void onStart() {
        Plugin plugin = pluginAccessor.getPlugin("your.plugin.key");
        final LoaderConfiguration config = new LoaderConfiguration(plugin);
        config.addPathsToAuxAtlassianPluginXMLs(
                "/dynamic/define-servlets.xml",
                "/dynamic/define-actions.xml"
        );

        if (isVersion(6)) {
            config.addPathsToAuxAtlassianPluginXMLs(
                    "/dynamic/v6-modules.xml"
            );
        }

        registrationHandle = dynamicModuleDescriptorFactory.loadModules(config);
    }

    void onEnd() {
        registrationHandle.unregister();
    }

    private boolean isVersion(int i) {
        throw new UnsupportedOperationException("Not implemented");
    }

    private DynamicModuleDescriptorFactory obtainFactory() {
        throw new UnsupportedOperationException("Not implemented");
    }
}
