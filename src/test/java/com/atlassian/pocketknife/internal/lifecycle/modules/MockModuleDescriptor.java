package com.atlassian.pocketknife.internal.lifecycle.modules;

import com.atlassian.plugin.ModuleCompleteKey;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

/**
 */
public class MockModuleDescriptor extends AbstractModuleDescriptor<Object>
{
    private final ModuleCompleteKey completeKey;

    public MockModuleDescriptor(final ModuleFactory moduleFactory, final String completeKey)
    {
        super(moduleFactory);
        this.completeKey = new ModuleCompleteKey(completeKey);
    }

    @Override
    public String getCompleteKey()
    {
        return completeKey.getCompleteKey();
    }

    @Override
    public String getPluginKey()
    {
        return completeKey.getPluginKey();
    }

    @Override
    public String getKey()
    {
        return completeKey.getModuleKey();
    }

    @Override
    public Object getModule()
    {
        return null;
    }
}
